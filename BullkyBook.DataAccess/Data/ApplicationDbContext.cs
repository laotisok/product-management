﻿using BullkyBook.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BullkyBook.DataAccess
{
    // IdentityDbContext instead of DbContext cause we want to use with scafflod identity
    public class ApplicationDbContext : IdentityDbContext
    {
        // shorcut: ctor+tab+tab
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        
        public DbSet<CoverType> CoverTypes { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Company> Companys { get; set; }

    }
}
