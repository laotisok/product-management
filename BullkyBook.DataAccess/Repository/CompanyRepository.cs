﻿using BullkyBook.DataAccess.Repository.IRepository;
using BullkyBook.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BullkyBook.Models;

namespace BullkyBook.DataAccess.Repository
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        private ApplicationDbContext _db;

        public CompanyRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        void ICompanyRepository.Update(Company obj)
        {
            _db.Companys.Update(obj);
        }
    }
}