﻿using BullkyBook.DataAccess;
using BullkyBook.DataAccess.Repository.IRepository;
using BullkyBook.Models;
using Microsoft.AspNetCore.Mvc;

namespace BullkyBookWeb.Controllers
{
    //Specify what controller belong to which area
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            IEnumerable<Category> objCategoryList = _unitOfWork.Category.GetAll();
            return View(objCategoryList);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category obj)
        {
            if(obj.Name == obj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("Name", "The Display order cannot exactly match the Name.");
            }

            if (ModelState.IsValid)
            {
                _unitOfWork.Category.Add(obj);
                _unitOfWork.Save();

                // TempData value only exist in one request
                TempData["success"] = "Category created successfully";

                return RedirectToAction("Index");
            }
            return View(obj);   
        }

        public IActionResult Edit(int? id)
        {
            if(id == null || id == 0)
            {
                return NotFound();
            }
            // sigle VS first: first can query and get many value but return only first one. single if have many value will throw exception.
            // if we know id is an primary key we can just use Find ()
            //var categorFromDb = _db.Categories.Find(id);
            var categorFromDb = _unitOfWork.Category.GetFirstOrDefault(u => u.Id == id);

            if (categorFromDb == null)
            {
                return NotFound();
            }

            return View(categorFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category obj)
        {
            if (obj.Name == obj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("Name", "The Display order cannot exactly match the Name.");
            }

            if (ModelState.IsValid)
            {
                _unitOfWork.Category.Update(obj);
                _unitOfWork.Save();

                TempData["success"] = "Category edited successfully";

                return RedirectToAction("Index");
            }
            return View(obj);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            //var categorFromDb = _db.Categories.Find(id);
            var categorFromDb = _unitOfWork.Category.GetFirstOrDefault(u => u.Id == id);


            if (categorFromDb == null)
            {
                return NotFound();
            }

            return View(categorFromDb);
        }

        [HttpPost, ActionName("DeletePost")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var categorFromDb = _unitOfWork.Category.GetFirstOrDefault(u => u.Id == id);

            if (categorFromDb == null)
            {
                return NotFound();
            }
            _unitOfWork.Category.Remove(categorFromDb);
            _unitOfWork.Save();

            TempData["success"] = "Category deleted successfully";

            return RedirectToAction("Index");
        }
    }
}
